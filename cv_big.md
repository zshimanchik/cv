﻿# Zakhar Shymanchyk
_Lead Software Engineer_

[zakshyman.dev](https://zakshyman.dev) |
[zakshyman@gmail.com](mailto:zakshyman@gmail.com) |
[GitLab](https://gitlab.com/zshimanchik) | 
[GitHub](https://github.com/zshimanchik) |
[LinkedIn](https://www.linkedin.com/in/zakshyman) |
[CV](https://zshimanchik.gitlab.io/cv/)


# Summary

Full-stack Software Engineer with 10+ years of experience using technology as a tool to solve complex business problems. Proficient in Python, TypeScript, FastAPI, NuxtJS and LLM with a strong ability to collaborate across teams to ensure seamless alignment between technical and business goals.

# Skills & Expertise

### Technical Skills
- **Languages**: Python, JavaScript, TypeScript, PHP, Bash
- **Backend**: FastAPI, Django, Flask, Laravel, SQLAlchemy, Celery
- **Frontend**: Vue.js, Nuxt, React, PWA
- **Databases**: PostgreSQL, MySQL, Redis, Weaviate, InfluxDB, RabbitMQ, MQTT
- **Cloud & DevOps**: AWS, Google Cloud (Certified Architect), Docker, Kubernetes, GitLab CI, Heroku, Railway
- **AI/ML**: LLM, RAG, OpenC
- **Infrastructure**: Nginx, uWSGI, Linux

### Leadership & Management Skills
- **Technical Leadership**: Led engineering teams, established Agile processes, hiring and team development
- **Startup Experience**: CTO role, technical strategy, team building, investor pitching
- **Performance Optimization**: System architecture, algorithms, data structures
- **Healthcare Software**: HIPAA compliance, Medical device development (IEC 62304, ISO 14971)

### Language Skills
- **English**: C1
- **Belarusian**: Native
- **Russian**: Native
- **Polish**: A2

# Work experience

- Jun 2024 - Nov 2024  **Freelance**
- Oct 2023 – Mar 2024  **Senior AI Engineer**       at **enneo.AI**      _Remote_
- May 2023 – Sep 2023  **Lead Software Engineer**   at **Forte Group**   _Remote_
- May 2020 – Aug 2022  **Lead Software Engineer**   at **Easee**         _in Amsterdam, Netherlands_
- Mar 2018 – Apr 2020  **Senior Software Engineer** at **EPAM Systems**  _in Amsterdam, Netherlands_
- Sep 2017 – Mar 2019  **Co-founder & CTO**         at **Smatsub**       _in Riga, Latvia_
- Nov 2016 – Sep 2017  **Senior Software Engineer** at **EPAM Systems**  _in Minsk, Belarus_
- Oct 2014 – May 2016  **Python/Java developer**    at **Exposit D.S.**  _in Minsk, Belarus_

# Education

### Yanka Kupala State University of Grodno (2012 - 2017)
- **Faculty/College**: Mathematics and Informatics
- **Degree (diploma)**: Specialist
- **Specialty**: Informatics

### Grodno State Professional Technical College of Light Industry (2009 - 2012)
- **Faculty/College**: Software Information Technology
- **Degree (diploma)**: Technical School Graduate
- **Specialty**: Computer Operator, Repair and Maintenance of Computing Machines Specialist

# Certifications

### Google Cloud Certified Professional Cloud Architect
- **Issued**: March 2020
- **Expires**: March 2022
- **URL**: [Credential](https://www.credential.net/d8621bb1-3ffc-4dd0-80f7-d615639cf8c3)

# Contributions

1. **Django**: [GitHub Repository](https://github.com/django/django)
   - Fixed a database-related bug

2. **Duckling**: [GitHub Repository](https://github.com/facebook/duckling)
   - Implemented AmountOfMoney dimension for the Russian language
   - Added new currencies
   - This service is used by [wit.ai](https://wit.ai/)

# Projects

## Freelance (June 2024 - November 2024)
**Project**: Chat bot based on LLM
**Project Role**: Python Backend Developer

**Task performed**:

- Implemented chat bot based on LLM with tooll calling
- Implemented functionality for the tools communicating with external services
- Deployed and supported in production
- Optimized cost usage of the API by implementing few-shot examples with cheaper model and fine-tuning.

**Environment**:

- Python, Flask, Gitlab-CI, Google Cloud, Railway


## Senior AI Engineer at Enneo.ai (October 2023 – March 2024)
**Project**: Customer service support with LLM (AI)
**Team Size**: 6 Developers
**Project Role**: Python Backend Developer  

**Tasks performed**:

- Improved stability of the service, speed of development and bugfixing by: refactoring, introducing best development practices, code quality control, automating then with CI
- Implemented "real-time" service for handling phone calls
- Implemented architecture of LLM for chat with RAG and tool calling
- Implemented testing of the chat LLM chat with LLM chat.

**Environment**:

- Python, FastAPI, Flask, Weaviate, Postgres, Gitlab-CI, Docker, PHP

## Lead Software Engineer at Fortegroup (May 2023 – September 2023)
**Project**: Fin-tech platform  
**Team Size**: 2 Developers, 1 PM, 1 Solution Architect  
**Project Role**: Lead Python Backend Developer  

**Tasks performed**:

- Analyzed the problems of existing technical solution and presented to the client
- Measured and optimized the performance of critical endpoints by 5-10%
- Introduced the best practices of development into existing solution
- Introduced testing for all of the functionality that was being changed
- Educated developers and "gradually enforced" best practices in automated way through CI automations.
- Improved package management, style autoformatting, static checkers, monitoring.

**Environment**:

- Python, Django, MySQL Gitlab-CI, Docker

## Lead Software Engineer at Easee (May 2020 – August 2022)
**Project**: Online Eye Test as a Medical Device  
**Team Size**: R&D team of 9-10 people  
**Project Role**: Full-Stack Developer  

**Tasks performed**:

- Established process for Engineering interviewing and hiring, educated other engineers to follow the process.
- Personal development plan, 1:1 and mentoring sessions with reportee.
- Shared knowledge with other team members via: creating wiki-pages, pair programming, code-review, organizing knowledge sharing sessions both internally and externally, invited speakers and trainers.
- Played role of the scrum master.
- Provided Tech-support for the partners and professional-users.
- Learned regulations in medical domain and participated in adjusting processes for compliance in software development and release process. IEC 62304, ISO 14971, ISO 13485, IEC 62366.
- Configured up-time monitoring of the services.
- Analyzed, prepared and implemented HIPAA requirements for the infrastructure migration, it includes configuration of services, changing pricing plans, signing BAA.
- Migrated applications and CI from Heroku to AWS services.
- Migrated from managed to self-hosted Sentry for HIPAA compliance.
- Introduced best-practices and managed AWS services/permissions and educated other engineers.

**Environment**:

- VueJS, Python, Flask, PHP, Laravel, Docker, AWS services, GitLab-CI.


## Senior Software Engineer at [EPAM Systems](http://www.epam.com) (Mar-2018 - May 2020)
**Customer**: European telecommunication company  
**Project**: Infrastructure stack for remote modem management  
**Team Size**: 3 Developers, 1 PM  
**Project Role**: Developer, responsible for component  

**Tasks performed**:

- On-site requirement analysis with tight customer communication
- Communication and management with 3d-party company which implemented additional functionality for ACS (Auto-configuration server)
- Leading a team of 3 developers
- Code-review, security-analysis, bugfix of 3d-party code 
- Design, estimation, and implementation of new sophisticated functionality in scope of ACS, considering lack of documentation and knowledge.
- Maintenance and support entire component, involving communication with number of departments.
- Prepared documentation and deployment packages for operational teams.
- Explain and provide insights for FE team regarding request flow.
- Performance testing
- Data analysis of historical data from several environments

**Environment**:

- MySQL
- Git, Jira, Trello
- Axiros ACS, Python


## CTO at Smartsub Inc (Sep 2018 - Mar 2019)
**Project**: Web Application to Create and Manage Google Ads Campaigns  
**Team Size**: 3 Developers, 1 QA, 1 UI/UX designer, 2 Product owners  
**Project Role**: CTO, Team-lead, PM, Full-stack Developer  

**Tasks performed**:

- Architecture design.
- People management, interviewing, hiring employees.
- Participation in startup accelerators, meetings with investors, fundraising.
- Team-leading, coordination.
- Established development-testing-release processes.
- Infrastructure maintenance.
- Gathering, analyzing user-behavior metrics and making decisions based on them.
- Implementation of backend component.
- Participating in implementation of frontend application.
- Implemented CRM, payment system using PayPal.
- Implemented CI using GitLab-CI.

**Environment**:

- Postgres, Nginx.
- Git, Trello, GitLab-CI.
- Python, Django, uWSGI, JS, React, React-Redux, Docker, docker-compose, k8s.


## CTO at Smartsub Inc (Dec-2016 - Jan-2018)
**Project**: Chat-bot focused on NLP to create and manage Google AdWords campaign. Bot analyze intent and extract data of every message, support different contexts with steps. Each context contains tree of steps with different handlers, what allows to create flexible conversation-like interface, handle commands with arguments from user, provide reminders and request actions from user.  
**Team Size**: 2 Developers, 2 Product owners  
**Project Role**: CTO, Team-lead, PM, Backend dev  

**Tasks performed**:

- Design, implementation and maintenance of a chat-bot for telegram and facebook using MicrosoftBotFramework
- Implementation of admin chat-bot 
- Intent analysis and data extraction using wit.ai service
- Contribution to the duckling.wit.ai project
- Integration with API implemented by another developer.
- Implemented CRM, payment system using PayPal
- Implemented FAQ functionality based on intent.
- Implemented CI using GitLab-CI
- Maintenance of production environments

**Environment**:

- Postgres, RabbitMQ, Nginx
- MicrosoftBotFramework, Wit.ai
- Git, Trello, GitLab-CI
- Python, Django, Celery, Docker, docker-compose


## Senior Software Developer at [EPAM Systems](http://www.epam.com) (Jun-2017 - Nov-2017)
**Customer**: Russian telecommunication company  
**Project**: High-loaded REST API server that must collect and manage promocodes. Promocodes could be several types: text promo-codes, links and QR-codes. Server also is responsible for generating QR-codes and uploading them to CDN in background, so tasks queue and scheduling also were implemented.  
**Team Size**: 4 Developers  
**Project Role**: Team Lead, Python Developer  

**Tasks performed**:

- Tight communications with customer's team
- Leading the team and task delegation.
- Design of project architecture
- Prepared docker-compose environment
- Implemented REST API business logic
- Code review other members of the team
- Worked with customer DevOps team to prepare project for deployment on customer environment using Jenkins

**Environment**:

- PostgreSQL, Redis, RabbitMQ
- Jenkins, PyCharm, Docker
- Python, Django, Django Rest Framework, Celery, AWS S3, Nginx, Docker, Docker-compose


## Senior Software Developer at [EPAM Systems](http://www.epam.com) (Jul-2017 - Sep-2017)
**Project**: Smart water valve IoT device for home usage with Amazon Alexa devices integration based on raspberry pi + arduino, with 3D printed body. Device also has sensor LCD display for showing information and configuration.  
**Team Size**:  4 Developers, 2 QA  
**Project Role**: Python Software-Engineer, Linux administrator  

**Tasks performed**:

- Took part in project architecture development
- Implemented business logic with Django
- Implemented GUI using PyGame on LCD display connected to raspberry
- Configured Linux for project (Systemd services)
- Implemented install scripts
- Prepared docker environment

**Environment**:

- SQLite
- Docker, PyCharm, Raspberry Pi 3 model B
- Python, Django, JQuery, Django Rest Framework, uWSGI, PyGame


## Software Engineer at [EPAM Systems](http://www.epam.com) (Mar-2017 - Mar-2017)
**Customer**: Cochlear  
**Project**: Pre-sale project. The main goal of the project was to prove ability to switch USB devices on demand of TestComplete tests between the virtual machines, which is running under VmWare WSXi using vSphere. In addition, make snapshots between the tests.
**Team Size**: 1 Deveveloper  
**Project Role**: Senior Software Engineer  

**Tasks performed**:

- Set up the VmWare EXSi and virtual machines under it.
- Implemented library for switching USB devices on demand and making snapshots
- Made TestComplete tests to check the presence of USB device
- Recorded a video for presentation
- Made a documentation for the project

**Environment**:

- Git
- VmWare WSXi, vSphere, TestComplete, Python.


## Software Engineer at [EPAM Systems](http://www.epam.com) (Feb-2017 - Mar-2017)
**Customer**: Cochlear  
**Project**: Pre-sale project for Cochlear Company. The main goal is to prove ability of programmatically from computer initiate searching and pairing with Bluetooth device on iPhone that connects to the computer using USB cable.  
**Team Size**: 1 iOS developer, 1 Back-end developer.  
**Project Role**: Senior Software Engineer  

**Tasks performed**:

- I have implemented python library, which is send commands to the iPhone device and retrieves information from it, using TCP sockets.
- Made demonstration video.
- Prepared project documentation.

**Environment**:

- Git
- Python, TCP sockets


## Software Engineer at [EPAM Systems](http://www.epam.com) (Jan-2017 - Feb-2017)
**Project**: Backend for clock project, implemented for gathering information from different sources, caching them and provide this information using REST-API to the client program.  
**Team Size**: 6 Back-end Developers, 1 Frond-end developer  
**Project Role**: Senior Software Engineer    

**Tasks performed**:

- Implemented functional for gathering and storing information from third-party services
- Took part in program design
- Performed review of merge requests
- Implemented Django core logic
- Performed manual testing and fixed bugs
- Code-review, onboarding of new developers.

**Environment**:

- PostgreSQL
- Git
- Docker, Django, Celery, Redis, RabbitMQ,


## Software Engineer at [EPAM Systems](http://www.epam.com) (Nov-2016 - Jan-2017)
**Project**: Automotive project created for sending sensors data from the car to the server then saving, analyzing and showing result. Also using this program car vendor could deploy and update car application and server-side application over the internet "on the fly".  
**Team Size**: 5 Back-end developers, 1 Front-end developer.  
**Project Role**: Software Engineer  

**Tasks performed**:

- Implemented car sensors emulator.
- Implemented car-side microservice for gathering and sending information to the server using TCP-sockets.
- Implemented server-side microservice for listening TCP-connections and saving income information to the InfluxDB.
- Configuration of InfluxDB
- Implemented part of Django backend API.
- Code-review, onboarding of new developers.

**Environment**:

- PostgreSQL, InfluxDB
- Git
- Python, QT, Django, Docker, Docker-compose, Docker-registry, Sockets, Nginx


## Personal project (Feb-2016 - Dec-2019)
**Project**: Emulation of forming food-seeking behavior based on artificial neural network in process of evolution. https://github.com/zshimanchik/animals  
**Project Role**: Python developer  

**Tasks performed**:

- Implemented Artificial neural network
- Implemented emulation
- Designed multiprocessing and computationally efficient architecture
- Tried different languages, drawing libraries and python interpreters

**Environment**:

- CPython, Qt, numpy, GTK, Jython, IronPython, WPF, Delphi, Java


## Freelance (Jun-2016 - Jul-2016)
**Customer**: individual entrepreneur  
**Project**: Multi user mobile game.  Users have some character and can create or join to the room for playing. Then they are playing regular round with each other in real time using websocket connection.  
**Team Size**: 1 Back-end developer, 1 Mobile developer, 1 Designer  
**Project Role**: Back-end developer  

**Tasks performed**:

- Designed and implemented architecture
- I have Implemented websocket server and logic for user connections
- Deployed the project to the server

**Environment**:

- MySQL
- Git
- Python, Django, Django-channels


## Software Engineer at Exposit D.S. (Apr-2016 - May-2016)
**Project**: Website - marketplace of trucks, built using PHP and Symphony 2  
**Team Size**: Dev team: 4 QA team: 1 Scrum master: 1  
**Project Role**: PHP Developer  

**Tasks performed**:

- Developed new functionality;
- Fixed bugs;
- Created and fixed website layout.

**Environment**:

- MySQL
- Git, Jenkins
- PHP, symphony 2


## Software Engineer at Exposit D.S. (Mar-2016 - Apr-2016)
**Project**: Corporate Web Chat with LDAP authentication. Supports audio/video calls and conferences using WebRTC technology.  
**Team Size**: 3 Full stack developers  
**Project Role**: Full stack developer  

**Tasks performed**:

- Implemented audio/video call functionality with usage of JS + WebRTC.

**Environment**:

- MySQL
- Git, Jenkins
- Java, Play Framework, Mustashe.js, JavaScript, WebRTC


## Software Engineer at Exposit D.S. (Sep-2015 - Mar-2016)
**Project**: Set of crawlers for automatic generation of leads. They grub public information from sites like linkedin, stackoverflow, monster.com and similar and integrate this data into Zoho CRM.  
**Team Size**: 2 Developers.  
**Project Role**: Software Engineer  

**Tasks performed**:

- Requirement analysis
- Designed application and DB
- Implemented continuous integration using Jenkins + Fabric 
- Implemented a number of crawlers, which grubs information from set of site (e.g. careers.stackoverflow.com)
- Implemented task system with dependencies and scheduling
- Implemented calculation and visualization of statistics
- Implemented functionality to integrate collected date to Zoho CRM
- Onboarded and delegated tasks to another developer.

**Environment**:

- Mysql
- Git, Jira, Jenkins
- Python, Django, xcrawler, Fabric


## Software Engineer at Exposit D.S. (Oct-2014 - Sep-2015)
**Customer**: apollon GmbH+Co. KG  
**Project**: Online Media Net (OMN)  Powerful online media tool for solutions in the following areas: Product Information Management, Media Asset Management, Brand Management, Workflow Management, Media Production Management. The basic modules with a powerful functionality are combined and configured in accordance with the processes of a specific customer into a single integral system of media production and management of multi-channel marketing of large companies  
**Team Size**: Several companies worked on this project. Our company's team consisted of about 20 people  
**Project Role**: Software Engineer  

**Tasks performed**:

- I have Implemented new functionality and fixed bugs for the modules on the frontend and on the backend sides.

**Environment**:

- Oracle, MSSQL
- Jira, Git, maven, tomcat, jetty, 
- J2EE (Maven, Spring, Hibernate, Tomcat, XML, Webservices), Red5 Server, Active MQ, Adobe Flex (ActionScript, BlazeDS, AIR), JS/CSS, NFS Helios, Oracle, Adobe Indesign Server, Quark Xpress Server


# Notes

- I'm open only to remote work, B2B via a Polish entity.
- An important factor for me to consider is whether I would need to pay VAT when receiving an invoice.

# Frequently asked questions

**Question**: What is your English level, verbal and writing?  
**Answer**: My English level, verbal and writing, is advanced. Last time it was tested in 2020 and estimated as C1. I was living in Amsterdam for 4.5 years where it was the main language to use. Since then, I travel a lot and use it in everyday life.

**Question**: What is your location?  
**Answer**: Currently, I have a residence permit and registered company in Poland to work B2B. My actual location may vary.

**Question**: What is your date of birth?  
**Answer**: April 1994.

**Question**: What are your salary expectations? | What is your expected rate per day?  
**Answer**: It highly depends on the role, challenge, technologies, and other factors of this position.

**Question**: When can you start in our company after the job offer?  
**Answer**: I'm available immediately.

**Question**: Expectations from the new position?  
**Answer**: I have three main criteria while choosing a new position. First is the challenge and role. Second is the domain and technologies. Third is the compensation. I will be considering a combination of these factors.

**Question**: Where are you from?  
**Answer**: Originally, I'm from Grodno, Belarus. Currently, I have a residence permit and registered company in Poland.
