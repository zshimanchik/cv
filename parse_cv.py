import re
import json
from typing import Optional, List, Dict
from dataclasses import dataclass, asdict
from pathlib import Path


@dataclass
class Project:
    role: str
    company: str
    company_link: Optional[str]
    description: str
    points: List[str]
    tech: List[str]
    date_period: str


# Skill categories and their corresponding Tailwind classes
SKILL_CATEGORIES = {
    # Languages
    "Python": "purple",
    "JavaScript": "purple",
    "TypeScript": "purple",
    "PHP": "purple",
    "Bash": "purple",
    "Java": "purple",
    # Backend
    "FastAPI": "blue",
    "Django": "blue",
    "Django-channels": "blue",
    "Flask": "blue",
    "Laravel": "blue",
    "SQLAlchemy": "blue",
    "Celery": "blue",
    "Django Rest Framework": "blue",
    "uWSGI": "blue",
    "Play Framework": "blue",
    "Axiros ACS": "blue",
    # Frontend
    "Vue.js": "pink",
    "VueJS": "pink",
    "Nuxt": "pink",
    "React": "pink",
    "PWA": "pink",
    "Redux": "pink",
    "JQuery": "pink",
    "Mustashe.js": "pink",
    # Data
    "Postgres": "amber",
    "PostgreSQL": "amber",
    "MySQL": "amber",
    "Redis": "amber",
    "Weaviate": "amber",
    "InfluxDB": "amber",
    "RabbitMQ": "amber",
    "MQTT": "amber",
    "SQLite": "amber",
    "AWS S3": "amber",
    # Cloud/DevOps
    "AWS": "cyan",
    "Google Cloud": "cyan",
    "GCP": "cyan",
    "Docker": "cyan",
    "Docker-compose": "cyan",
    "Docker-registry": "cyan",
    "Kubernetes": "cyan",
    "k8s": "cyan",
    "Gitlab-CI": "cyan",
    "GitLab-CI": "cyan",
    "GitLab CI": "cyan",
    "Jenkins": "cyan",
    "Heroku": "cyan",
    "Railway": "cyan",
    "Linux": "cyan",
    "Nginx": "cyan",
    "Raspberry Pi": "cyan",
    # AI/ML
    "LLM": "green",
    "RAG": "green",
    "OpenCV": "green",
    "wit.ai": "green",
}

DEFAULT_COLOR = "indigo"  # A bright, distinct color for unknown technologies


def get_tech_color(tech: str) -> str:
    """Get the color category for a technology, using default color if not found."""
    if tech not in SKILL_CATEGORIES:
        print(
            f"Warning: Technology '{tech}' not found in SKILL_CATEGORIES. Using default color."
        )
        return DEFAULT_COLOR
    return SKILL_CATEGORIES[tech]


def parse_projects(content: str) -> List[Project]:
    # Find the Projects section
    projects_section = re.search(r"# Projects\n\n(.*?)(?=\n#\s|$)", content, re.DOTALL)
    if not projects_section:
        return []

    projects_text = projects_section.group(1)

    # Split into individual projects
    project_blocks = re.split(r"\n##\s+", projects_text.strip())

    projects = []
    for block in project_blocks:
        if not block.strip():
            continue
        block = block.lstrip("#")

        # Parse project header
        header_match = re.match(
            r"(?P<role>.*?),\s*(?:\[(?P<company_name>.*?)\]\((?P<company_link>.*?)\)|(?P<company_plain>.*?))\s*\((?P<date_period>.*?)\)",
            block.split("\n")[0],
        )

        if not header_match:
            raise ValueError(f"Failed to parse header: {block.split('\n')[0]}")

        # Get company details
        company = header_match.group("company_name") or header_match.group(
            "company_plain"
        )
        company_link = header_match.group("company_link")
        role = header_match.group("role").strip()
        date_period = header_match.group("date_period").strip()

        # Split content into lines
        lines = block.split("\n")[1:]

        # Get description (first line after header)
        description = ""
        points = []
        tech = []

        current_section = "description"
        for line in lines:
            line = line.strip()
            if not line:
                continue

            if line.startswith("- "):
                current_section = "points"
                points.append(line[2:])
            elif line.startswith("**Tech**:"):
                current_section = "tech"
                tech = [t.strip() for t in line.replace("**Tech**:", "").split(",")]
            elif current_section == "description":
                description = line

        projects.append(
            Project(
                role=role,
                company=company,
                company_link=company_link,
                description=description,
                points=points,
                tech=tech,
                date_period=date_period,
            )
        )

    return projects


def generate_html(projects: List[Project]) -> str:
    html_projects = []

    for i, project in enumerate(projects):
        # Convert tech list to HTML spans
        tech_spans = []
        for tech in project.tech:
            tech = tech.strip()
            if not tech:
                continue

            color = get_tech_color(tech)
            tech_span = f"""
                <span class="px-3 py-0.5 bg-{color}-800/50 text-{color}-200 rounded-full">{tech}</span>"""
            tech_spans.append(tech_span)

        # Generate points HTML
        points_html = "\n".join([f"<li>{point}</li>" for point in project.points])

        # Company link or plain text
        company_html = (
            f'<a href="{project.company_link}" class="hover:text-green-400">{project.company}</a>'
            if project.company_link
            else project.company
        )

        # Set aria-hidden and show_more_text based on project index
        aria_hidden = "false" if i < 4 else "true"
        show_more_text = "" if i < 4 else "show more"

        project_html = f"""
                <div class="border border-gray-700 p-6 rounded-lg hover:shadow-lg hover:border-green-400 transition">
                    <div class="flex justify-between items-start mb-2 cursor-pointer group" onclick="toggleProject(this)">
                        <h3 class="text-2xl font-semibold">
                            {project.role}
                            <span class="text-xl text-gray-400">@ {company_html}</span>
                        </h3>
                        <div class="flex items-center gap-4">
                            <span class="text-gray-400 text-sm">{project.date_period}</span>
                            <span class="text-sm text-gray-400 hover:text-green-400 transition-colors underline show-more-link">{show_more_text}</span>
                        </div>
                    </div>
                    <div class="project-details" aria-hidden="{aria_hidden}">
                        <p class="text-gray-300 mb-2">{project.description}</p>
                        <ul class="list-disc list-inside text-gray-400 space-y-1 mb-3">
                            {points_html}
                        </ul>
                    </div>
                    <div class="flex flex-wrap gap-1.5">
                        {''.join(tech_spans)}
                    </div>
                </div>"""

        html_projects.append(project_html)

    return "\n".join(html_projects)


def update_html_projects(cv_html_content: str, projects_html: str) -> str:
    """
    Update the Work Experience section in the HTML content with new projects.

    Args:
        cv_html_content: The full HTML content of cv.html
        projects_html: The generated HTML for projects

    Returns:
        Updated HTML content with new projects inserted
    """
    # Pattern to match the entire Work Experience section's content
    pattern = r'(<!-- Work Experience Section -->.*?<div class="space-y-2">\n).*?(\s*</div>\s*</section>)'

    # Replace the content using regex
    updated_html = re.sub(
        pattern, f"\\1{projects_html}\\2", cv_html_content, flags=re.DOTALL
    )

    return updated_html


def main():
    cv_path = Path("cv.md")
    content = cv_path.read_text()

    projects = parse_projects(content)
    print("Found", len(projects), "projects")
    projects_html = generate_html(projects)

    # Read and update cv.html
    cv_html_path = Path("cv.html")
    cv_html_content = cv_html_path.read_text()
    updated_html = update_html_projects(cv_html_content, projects_html)

    # Write the updated content back to cv.html
    cv_html_path.write_text(updated_html)
    print("Successfully updated cv.html with new projects")


if __name__ == "__main__":
    main()
