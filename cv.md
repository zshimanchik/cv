﻿# Zakhar Shymanchyk
_Lead Software Engineer_

[zakshyman.dev](https://zakshyman.dev) |
[zakshyman@gmail.com](mailto:zakshyman@gmail.com) |
[GitLab](https://gitlab.com/zshimanchik) | 
[GitHub](https://github.com/zshimanchik) |
[LinkedIn](https://www.linkedin.com/in/zakshyman) |
[CV](https://zshimanchik.gitlab.io/cv/)


# Summary

Full-stack Software Engineer with 10+ years of experience using technology as a tool to solve complex business problems. Proficient in Python, TypeScript, FastAPI, NuxtJS and LLM with a strong ability to collaborate across teams to ensure seamless alignment between technical and business goals.

# Skills & Expertise

### Technical Skills
- **Languages**: Python, JavaScript, TypeScript, PHP, Bash
- **Backend**: FastAPI, Django, Flask, Laravel, SQLAlchemy, Celery
- **Frontend**: Vue.js, Nuxt, React, PWA
- **Databases**: PostgreSQL, MySQL, Redis, Weaviate, InfluxDB, RabbitMQ, MQTT
- **Cloud & DevOps**: AWS, Google Cloud (Certified Architect), Docker, Kubernetes, GitLab CI, Heroku, Railway
- **AI/ML**: LLM, RAG, OpenC
- **Infrastructure**: Nginx, uWSGI, Linux

### Leadership & Management Skills
- **Technical Leadership**: Led engineering teams, established Agile processes, hiring and team development
- **Startup Experience**: CTO role, technical strategy, team building, investor pitching
- **Performance Optimization**: System architecture, algorithms, data structures
- **Healthcare Software**: HIPAA compliance, Medical device development (IEC 62304, ISO 14971)

### Language Skills
- **English**: C1
- **Belarusian**: Native
- **Russian**: Native
- **Polish**: A2

# Work experience

- Jun 2024 - Nov 2024  **Freelance**
- Oct 2023 – Mar 2024  **Senior AI Engineer**       at **enneo.AI**      _Remote_
- May 2023 – Sep 2023  **Lead Software Engineer**   at **Forte Group**   _Remote_
- May 2020 – Aug 2022  **Lead Software Engineer**   at **Easee**         _in Amsterdam, Netherlands_
- Mar 2018 – Apr 2020  **Senior Software Engineer** at **EPAM Systems**  _in Amsterdam, Netherlands_
- Sep 2017 – Mar 2019  **Co-founder & CTO**         at **Smatsub**       _in Riga, Latvia_
- Nov 2016 – Sep 2017  **Senior Software Engineer** at **EPAM Systems**  _in Minsk, Belarus_
- Oct 2014 – May 2016  **Python/Java developer**    at **Exposit D.S.**  _in Minsk, Belarus_

# Education

### Yanka Kupala State University of Grodno (2012 - 2017)
- **Faculty/College**: Mathematics and Informatics
- **Degree (diploma)**: Specialist
- **Specialty**: Informatics

### Grodno State Professional Technical College of Light Industry (2009 - 2012)
- **Faculty/College**: Software Information Technology
- **Degree (diploma)**: Technical School Graduate
- **Specialty**: Computer Operator, Repair and Maintenance of Computing Machines Specialist

# Certifications

### Google Cloud Certified Professional Cloud Architect
- **Issued**: March 2020
- **Expires**: March 2022
- **URL**: [Credential](https://www.credential.net/d8621bb1-3ffc-4dd0-80f7-d615639cf8c3)

# Contributions

1. **Django**: [GitHub Repository](https://github.com/django/django)
   - Fixed a database-related bug

2. **Duckling**: [GitHub Repository](https://github.com/facebook/duckling)
   - Implemented AmountOfMoney dimension for the Russian language
   - Added new currencies
   - This service is used by [wit.ai](https://wit.ai/)

# Projects

## Senior AI Engineer, Freelance (Jun 2024 - Nov 2024)
LLM-based chat bot with tool calling capabilities
- Implemented and deployed production chat bot with external service integration
- Optimized API costs through few-shot examples and model fine-tuning
**Tech**: Python, Flask, Google Cloud, Railway, GitLab-CI


## Senior AI Engineer, [Enneo.ai](https://enneo.ai) (Oct 2023 – Mar 2024)
Customer service AI support system | Team: 8 devs
- Improved service stability through refactoring and CI automation
- Built real-time phone call handling service
- Implemented RAG-based LLM chat architecture with tool calling
- Developed LLM-based testing framework
**Tech**: Python, FastAPI, Flask, Weaviate, Postgres, Docker, GitLab-CI, PHP


## Lead Software Engineer, [Fortegroup](https://fortegrp.com/) (May 2023 – Sep 2023)
Fin-tech investment platform | Team: 4 members
- Optimized critical endpoints performance by 5-10%
- Introduced automated testing and CI best practices
- Led technical analysis and improvement initiatives
- Educated developers and "gradually enforced" best practices
**Tech**: Python, Django, MySQL, Redis, GitLab-CI, Docker


## Lead Full Stack Software Engineer, [Easee](https://easee.online) (May 2020 – August 2022)
Online Eye Test as a Software Medical Device | Team: 10 people
- Established process for Engineering interviewing and hiring, educated other engineers to follow the process.
- Personal development plan, 1:1 and mentoring sessions with reportee.
- Shared knowledge with other team members via: creating wiki-pages, pair programming, code-review, organizing knowledge sharing sessions both internally and externally, invited speakers and trainers.
- Played role of the scrum master.
- Provided Tech-support for the partners and professional-users.
- Learned regulations in medical domain and participated in adjusting processes for compliance in software development and release process. IEC 62304, ISO 14971, ISO 13485, IEC 62366.
- Configured up-time monitoring of the services.
- Analyzed, prepared and implemented HIPAA requirements for the infrastructure migration, it includes configuration of services, changing pricing plans, signing BAA.
- Migrated applications and CI from Heroku to AWS services.
- Migrated from managed to self-hosted Sentry for HIPAA compliance.
- Introduced best-practices and managed AWS services/permissions and educated other engineers.
**Tech**: VueJS, Python, Flask, PHP, Laravel, Docker, AWS, GitLab-CI


## Senior/Lead Software Engineer, [EPAM Systems](https://www.epam.com) (Mar-2018 - May 2020)
Infrastructure stack for remote modem management | Team: 3 + 21
- Led team of 3 developers, managed 3rd-party vendor relationships
- Performed on-site requirement analysis and technical design
- Reduced dependency on vendors by hiring and onboarding 2 new developers
- Implemented new functionality for Auto-configuration server (ACS) through reverse engineering
- Conducted code reviews, security analysis, and performance optimization 
- Assisted with technical interviews and hiring across multiple teams
- Prepared documentation and deployment packages for operational teams
**Tech**: Python, Axiros ACS, MySQL, Git, Jira, Trello


## Lead Software Engineer & CTO, Smartsub Inc (Sep 2018 - Mar 2019)
Web Application to Create and Manage Google Ads Campaigns | Team: 3 devs, 1 QA, 1 UI/UX, 2 POs
- Architecture design
- People management, interviewing, hiring employees
- Participation in startup accelerators, meetings with investors, fundraising
- Team-leading, coordination
- Established development-testing-release processes
- Infrastructure maintenance
- Gathering, analyzing user-behavior metrics and making decisions based on them
- Implementation of backend component
- Participating in implementation of frontend application
- Implemented CRM, payment system using PayPal
- Implemented CI using GitLab-CI
**Tech**: Python, Django, React, Redux, PostgreSQL, Docker, k8s, GitLab-CI


## Lead Software Engineer & CTO, Smartsub Inc (Dec 2016 - Jan 2018)
NLP-based chat bot for Google AdWords campaign management | Team: 2 devs, 2 POs
- Designed and implemented multi-platform chat bot using MicrosoftBotFramework
- Built intent analysis system with wit.ai for natural language processing
- Contributed to Facebook's duckling.wit.ai project
- Implemented CRM and PayPal payment integration
- Set up CI/CD pipeline and managed production infrastructure
**Tech**: Python, Django, wit.ai, MicrosoftBotFramework, Celery, PostgreSQL, RabbitMQ, Docker, GitLab-CI


## Senior/Lead Software Developer, [EPAM Systems](https://www.epam.com) (Jun-2017 - Nov-2017)
High-loaded REST API service for promo-codes | Team: 4 devs
- Tight communications with customer's team
- Leading the team and task delegation.
- Design of project architecture
- Prepared docker-compose environment
- Implemented REST API business logic
- Code review other members of the team
- Worked with customer DevOps team to prepare project for deployment on customer environment using Jenkins
**Tech**: Python, Django, DRF, Celery, PostgreSQL, Redis, RabbitMQ, AWS S3, Docker, Jenkins


## Senior Software Developer, [EPAM Systems](https://www.epam.com) (Jul-2017 - Sep-2017)
Smart water valve IoT device for home usage with Amazon Alexa devices integration based on raspberry pi + arduino, with 3D printed body. Device also has sensor LCD display for showing information and configuration. | Team: 4 Devs, 2 QA
- Took part in project architecture development
- Implemented business logic with Django
- Implemented GUI using PyGame on LCD display connected to raspberry
- Configured Linux for project (Systemd services)
- Implemented install scripts
- Prepared docker environment
**Tech**: Python, Django, JQuery, Django Rest Framework, uWSGI, PyGame, SQLite, Docker, Raspberry Pi


## Software Engineer, [EPAM Systems](https://www.epam.com) (Mar-2017 - Mar-2017)
Pre-sale project. The main goal of the project was to prove ability to switch USB devices on demand of TestComplete tests between the virtual machines, which is running under VmWare WSXi using vSphere. In addition, make snapshots between the tests. | Team: 1 dev
- Set up the VmWare EXSi and virtual machines under it.
- Implemented library for switching USB devices on demand and making snapshots
- Made TestComplete tests to check the presence of USB device
- Recorded a video for presentation
- Made a documentation for the project
**Tech**: Python, VmWare WSXi, vSphere, TestComplete, Git


## Software Engineer, [EPAM Systems](https://www.epam.com) (Feb-2017 - Mar-2017)
Pre-sale project for Cochlear Company. The main goal is to prove ability of programmatically from computer initiate searching and pairing with Bluetooth device on iPhone that connects to the computer using USB cable. | Team: 1 BE dev, 1 iOS dev
- I have implemented python library, which is send commands to the iPhone device and retrieves information from it, using TCP sockets.
- Made demonstration video.
- Prepared project documentation.
**Tech**: Python, TCP sockets, Git


## Software Engineer, [EPAM Systems](https://www.epam.com) (Jan-2017 - Feb-2017)
Backend for clock project, implemented for gathering information from different sources, caching them and provide this information using REST-API to the client program. | Team: 6 BE Devs, 1 FE Dev
- Implemented functional for gathering and storing information from third-party services
- Took part in program design
- Performed review of merge requests
- Implemented Django core logic
- Performed manual testing and fixed bugs
- Code-review, onboarding of new developers.
**Tech**: Python, Django, Celery, PostgreSQL, Redis, RabbitMQ, Docker, Git


## Software Engineer, [EPAM Systems](https://www.epam.com) (Nov-2016 - Jan-2017)
Automotive project created for sending sensors data from the car to the server then saving, analyzing and showing result. Also using this program car vendor could deploy and update car application and server-side application over the internet "on the fly". | Team: 5 BE Devs, 1 FE Dev
- Implemented car sensors emulator.
- Implemented car-side microservice for gathering and sending information to the server using TCP-sockets.
- Implemented server-side microservice for listening TCP-connections and saving income information to the InfluxDB.
- Configuration of InfluxDB
- Implemented part of Django backend API.
- Code-review, onboarding of new developers.
**Tech**: PostgreSQL, InfluxDB, Git, Python, QT, Django, Docker, Docker-compose, Docker-registry, Sockets, Nginx


## Had Fun, [Personal project](https://github.com/zshimanchik/animals) (Feb-2016 - Dec-2019)
Emulation of forming food-seeking behavior based on artificial neural network in process of evolution. https://github.com/zshimanchik/animals  
- Implemented Artificial neural network
- Implemented emulation
- Designed multiprocessing and computationally efficient architecture
- Tried different languages, drawing libraries and python interpreters
**Tech**: CPython, Qt, numpy, GTK, Jython, IronPython, WPF, Delphi, Java


## Python Software Engineer, Freelance (Jun-2016 - Jul-2016)
Multi user mobile game.  Users have some character and can create or join to the room for playing. Then they are playing regular round with each other in real time using websocket connection. | Team 1 BE dev, 1 Mobile dev, 1 Designer
- Designed and implemented architecture
- I have Implemented websocket server and logic for user connections
- Deployed the project to the server
**Tech**: MySQL, Git, Python, Django, Django-channels


## Software Engineer, [Exposit](https://www.exposit.com) (Apr-2016 - May-2016)
Marketplace of trucks, built using PHP and Symphony 2 | Team: 4 dev, 1 QA, 1 Scrum master
- Developed new functionality;
- Fixed bugs;
- Created and fixed website layout.
**Tech**: MySQL, Git, Jenkins, PHP, symphony 2


## Software Engineer, [Exposit](https://www.exposit.com) (Mar-2016 - Apr-2016)
Corporate Web Chat with LDAP authentication. Supports audio/video calls and conferences using WebRTC technology. | Team: 3
- Implemented audio/video call functionality with usage of JS + WebRTC.
**Tech**: JavaScript, WebRTC, Mustashe.js, Jenkins, Java, Play Framework, MySQL, Git


## Software Engineer, [Exposit](https://www.exposit.com) (Sep-2015 - Mar-2016)
Set of crawlers for automatic generation of leads. They grub public information from sites like linkedin, stackoverflow, monster.com and similar and integrate this data into Zoho CRM. | Team: 2
- Requirement analysis
- Designed application and DB
- Implemented continuous integration using Jenkins + Fabric 
- Implemented a number of crawlers, which grubs information from set of site (e.g. careers.stackoverflow.com)
- Implemented task system with dependencies and scheduling
- Implemented calculation and visualization of statistics
- Implemented functionality to integrate collected date to Zoho CRM
- Onboarded and delegated tasks to another developer.
**Tech**: Mysql, Git, Jira, Jenkins, Python, Django, xcrawler, Fabric


## Software Engineer, [Exposit](https://www.exposit.com) (Oct-2014 - Sep-2015)
Online Media Net (OMN) Powerful online media tool for solutions in the following areas: Product Information Management, Media Asset Management, Brand Management, Workflow Management, Media Production Management. The basic modules with a powerful functionality are combined and configured in accordance with the processes of a specific customer into a single integral system of media production and management of multi-channel marketing of large companies | Team: 20 people
- I have Implemented new functionality and fixed bugs for the modules on the frontend and on the backend sides.
**Tech**: Java, JavaScript, ActionScript, J2EE, Maven, Spring, Hibernate, Tomcat, Jetty, XML, Webservices, Oracle, MSSQL, Git, Red5 Server, Active MQ, Adobe Flex, BlazeDS, AIR, Adobe Indesign Server, Quark Xpress Server


# Frequently asked questions

**Question**: What is your preferred work arrangement?  
**Answer**: I'm open only to remote work, B2B via a Polish entity.

**Question**: What is your location?  
**Answer**: Currently, I have a residence permit and registered company in Poland to work B2B. My actual location may vary.

**Question**: What is your date of birth?  
**Answer**: April 1994.

**Question**: What are your salary expectations? | What is your expected rate per day?  
**Answer**: It highly depends on the role, challenge, technologies, and other factors of this position.

**Question**: When can you start in our company after the job offer?  
**Answer**: I'm available immediately.

**Question**: Expectations from the new position?  
**Answer**: When evaluating new opportunities, I focus on three key aspects:
1. Professional Growth: Seeking roles that offer meaningful challenges and leadership opportunities
2. Technical Environment: Looking for projects utilizing modern technologies and following industry best practices
3. Competitive Compensation: Expecting a package that reflects my experience and market value

**Question**: Where are you from?  
**Answer**: Originally, I'm from Grodno, Belarus. Currently, I have a residence permit and registered company in Poland.
